Zig modernizes C, one of the worlds more popular programming languages.  C was developed in the 1970's and It shows it's age.  Computer science has moved forward and it is time to get rid of some of the baggage that C has.  The way to do it is to create a language without the baggage.

C is popular become it is a simple, fast and powerful language.  When building extensions for other languages, like Ruby or Python, C is the de facto standard because it is fast and everywhere.  Need to write code memory effective code for a microwave?  Turn to C.

Zig, like C, is good for low level programming like systems programming, device drivers, kernal programming and embedded devices.

Zig offers the same benefits of C without the baggage.  It is easier to write safe and good code in Zig.  Here is a list of improvements Zig has:

- C interoperability
- Compiles and runs faster than C [^https://www.youtube.com/watch?v=Z4oYSByyRak]]  
- Produces faster machine code than C
- Improved Memory Management Allocators
- Test framework
- Safe string handling
- Errors must be handle robustly and ergonomically
- Compile time code execution and reflection
- Ships with a build system
- Package management
- Namespaces
- Generics and Meta Programming
- Out of the box cross compilation to supported platforms (zen)
Zig gives the programmer full control, which can be a heavy responsibilty.
## C Interoperability
%% TOOD: Add link to section %%
Zig will compile to the standard, so libraries created with it are a drop in replacement for code created with C.

C code can be compiled in Zig, so C functions can be written in C.  The C library can be used.  This is a big help in migrating to Zig/

Put extern in front of Struct, Enum, Union and Functions and it will work with the C ABI.
## Test framework
%% TOOD: Add link to section %%
Zig has a built in test framework that allows the same file to contain the source code and tests the prove the code works.
## String handling
%% TOOD: Add link to section %%
String overflows are a big problem in C.  By default strings in C contain both the data and the length of the string.  If there is a string overrun attempt, the compiler or the runtime code will squawk.  
## Memory Management
%% TOOD: Add link to section %%
Zig has a number of Allocators, which are used for memory management.  At first this looks like a bookkeeping headache, but it allows for greater flexibility.  C only has one, `malloc`.

Testing how C code response to an out of memory issue, is difficult at best.  In Zig it is possible to pass it the Test Allocator, which will report that error and test the error handling.

%% TODO: provide more examples of allocators %%

In C there is a lot of code between a `malloc` and `free` which means it is easy to forget an `free` and difficult to audit.  Zig has a `defer` keyword which allows allocation code and clean up code to be close together, solving both problems with a clean and elegant solution
## Error handing
%% TOOD: Add link to section %%
In C the path of least resistance is to ignore errors.  In Zig you must always acknowledge them, even if you decide to ignore them, the code shows that they may exist.

Zig has "Error Groups" which are returned by a function, notifying the caller that they exist.  This brings error handling to the forefront of the developers mind.
## Build System
%% TOOD: Add link to section %%
Zig comes with a build system cooked in.  There is no need for learning another programming language to compile a project, nor is there the knobs temptation to hand roll a bunch of unmaintainable set of build scripts.

Zig's build system can cross compile.  It can create a Windows dynamic link library (dll) on a Linux machine visa versa.  It can even create Web Assembly for use in the browser.
## Package management
%% TODO: Improve %%
%% TOOD: Add link to section %%
There is no standard package manager in C, which means there is a temptation to vendor library code that will never be updated.

Zig has a package manager that allows libraries to be pulled in from git repositories like github, gitlab or codeberg. 
## Namespaces
%% TODO: Improve %%
%% TOOD: Add link to section %%
Name collision is a concern in C.  Is the common sense name I use for my function also the common sense name someone else uses?  Namespacing is the perfect solution and Zig comes with it built in

## Generics and Meta Programming


