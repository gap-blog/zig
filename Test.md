%%
TODO: Read https://mtlynch.io/notes/zig-unit-test-c/
TODO Read https://ziglang.org/documentation/master/std/#std.testing




%%
## Pass a test

```
// src/always_pass.zig
const std = @import("std");
const expect = std.testing.expect;

test "always succeeds" {
    try expect(true);
}
```
To build and run
```
zig init
code src/always_pass.zig
zig build
zig test src/always_pass.zig
```
It will respond with
```
All 1 tests passed.
```
## Fail a test
```
// src/always_fail.zig
const std = @import("std");
const expect = std.testing.expect;

test "always succeeds" {
    try expect(false);
}
```
To build and run
```
zig init
code src/always_fail.zig
zig build
zig test src/always_fail.zig

It will respond with:

```

```
Test [1/1] always_fail.test.always fails... FAIL (TestUnexpectedResult)
/home/gap/.zvm/master/lib/std/testing.zig:540:14: 0x1038e1f in expect (test)
    if (!ok) return error.TestUnexpectedResult;
             ^
/home/gap/zig/test/src/always_fail.zig:5:5: 0x1038f35 in test.always fails (test)
    try expect(false);
    ^
0 passed; 0 skipped; 1 failed.
error: the following test command failed with exit code 1:
/home/gap/zig/test/zig-cache/o/cb23d4f8771a4541cfa7ba39657e5daf/test

```


## Testing a C function

%% TODO %%



## Skipping a test
https://ziglang.org/documentation/master/#Skip-Tests

## Detecting a test build
https://ziglang.org/documentation/master/#toc-Detecting-Test-Build

## TODO 

From TODO Read https://ziglang.org/documentation/master/std/#std.testing
- checkAllAllocationFailures(backing_allocator: std.mem.Allocator, comptime test_fn: anytype, extra_args: anytype)
- expect(ok: bool)
- expectApproxEqAbs(expected: anytype, actual: anytype, tolerance: anytype)
- expectApproxEqRel(expected: anytype, actual: anytype, tolerance: anytype)
- expectEqual(expected: anytype, actual: anytype)
- expectEqualDeep(expected: anytype, actual: anytype) error{TestExpectedEqual}
-  expectEqualSentinel(comptime T: type, comptime sentinel: T, expected: [:sentinel]const T, actual: [:sentinel]const T) 
- expectEqualSlices(comptime T: type, expected: []const T, actual: []const T)
- expectEqualStrings(expected: []const u8, actual: []const u8)
- expectError(expected_error: anyerror, actual_error_union: anytype)
- expectFmt(expected: []const u8, comptime template: []const u8, args: anytype)
- expectStringEndsWith(actual: []const u8, expected_ends_with: []const u8)
- expectStringStartsWith(actual: []const u8, expected_starts_with: []const u8)
- refAllDecls(comptime T: type)
-  refAllDeclsRecursive(comptime T: type)
-  tmpDir(opts: std.fs.Dir.OpenDirOptions) TmpDir
