## Structs 

- Zig in Depth： Memory Layout

```zig
const std = @import("std)

// Compatiable with C, but pads each member to ensure alignment, ie fit the largest's size (b)
const Extern = extern struct {
	a: u16, // natuaral align 2
	b: u64, // natuaral align 8
	c: u16, // natural align 2
};

// compiler decides padding and alighment
const Noraml = struct {
	a: u16, 
	b: u64, 
	c: u16, 
};

// Retains order, but no padding
const Packed = packed struct {
	a: u16, 
	b: u64, 
	c: u16, 
};

pub fn main() !void {
	printInfo(Extern);
	printInfo(Normal);
	printInfo(Packed);
	// Packed structs can be bit casted
	const w = Whole{
		.num = 0x1234,
	};

	const p: Parts = @bitCast(w);
	std.debug.print("w num: 0x{x}\n, .{w.num});
	std.debug.print("p.half: 0x{x}\n, .{p.half});
	std.debug.print("p.q3: 0x{x}\n, .{p.q3});
	std.debug.print("p.q4: 0x{x}\n, .{p.q4});
	
}


fn printInfo(comptime T: type) void {
	std.debug.print("Size of [s]: {}\n", .{ @typeName(T), @sizeof(T) });

	inline for (std.meta.fields(t)) |field| {
		std.debug.print("  field {s} byte offset: {}\n", .{ field.name, @offsetOf(T, field.name) });
	}
std.debug.print("\n", .{})
}

const Whole = packed struct {
	num: u16,
};

const Parts = packed struct {
	half: u8,
	q3: u4,
	q4: u4,
};

```
