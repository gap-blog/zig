
`const name: type = value`
`var name: type: = value`

Will do type inference (`comptime_int` is the specal type of consts assign untyped int, likewise `comptime_float`)

variable: snake_case
function: camelCase

`var x = 3` gives an error because it needs a type.  Everything must be initialized, can use undefined.  Compiler does not protect you.
Must use variables.  Can use `_ = var`

Unsigned: u8, u16, u32, u64, u128, usize (size of the Architecture, u(-65535))
Signed: i8, i16, i32, i64, i128, isize
Literals can be decimal, binary, hex or octal
```

_ = 1_000_000 ;// decimal
_ = 0x10ff_ffff; // hex
_ = 0x777; //octal
_ = 0b1111_0101_0111; //binary
var v: u1 = 0;
// This would error
// v = 2;

const unicode: u21 = 'x';
 ```
Floating point
f16, f32, f64, f80, f128, 

```
_ = 123.0E+77 //With exponent
_ = 0x123.456 // Hex
_ = 0x123.456p-3 // Hex with exponent, use p since e is a digit

_ = std.math.inf(f64); // Infinity
_ = std.math.inf(f64); // Negative infinity
_ = std.math.nan(f64); // Not a number
```

```

fn printInfo(name: []const u8, x:anytype) : void {
	std.debug.print("{s:>10} {any: ^10}\t{}\n" .{name, x, @TypeOf(x)});
}

std.debug.print("{s:>10} {s: ^10}\t{s}\n" .{"name", "value", "type"});
std.debug.print("{s:>10} {s: ^10}\t{s}\n" .{"____", "____", "____"});

const a = 1;
printInfo("a", a);

```


