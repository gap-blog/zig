This is the outline of the book [C Programming in Easy Steps](https://kcls.bibliocommons.com/item/show/1900327082)
# 1) Getting Started @c
## Introducing the C langauge @c
## Installing C @c
## Writing a C program @c
## Compiling a C program @c
## Understanding compilation @c
## Summary @c

# 2) Storing Variable Values @c
## Creating program variables @c
## Displaying variable values @c
## Inputting variable values @c
## Qualifying data types @c
## Using Global variables @c
## Registering variables @c
## Conversion data types @c
## Creating array variabler @c
## Describing dimensions @c
## Summary @c

# 3) Setting Constant Values @c
## Declaring program constants @c
## Enumerating constant values @c
## Creating a constant type @c
## Defining constants @c
## Debugging definitions @c
## Summary @c
# 4) Performing Operations
## Doing arithmetic @c
## Assigning values @c
## Comparing values @c
## Assessing logic @c
## Examining conditions @c
## Measuring size @c
## Comparing bit values @c
## Flagging bits @c
## Understanding precedence @c
## Summary @c

# 5) Making Statements @c
## Testing expressions @c
## Branching switches @c
## Looping for a number @c
## Looping while true @c
## Breaking out of loops @c
## Going to labels @c
## Summary @c
# 6) Employing functions @c
## Declaring functions @c
## Supplying arguments @c
## Calling recursively @c
## Placing functions in headers @c
## Restricting accessibility @c
## Summary @c
# 7) Pointing to data @c
## Accessing data via pointers @c
## Doing pointer arithmetic @c
## Passing pointers to functions @c
## Creating arrays of pointers @c
## Pointing to functions @c
## Summary ## @c
# 8) Manipulating Strings @c
## Reading strings @c
## Coping strings @c
## Joining strings @c
## Finding substings @c
## Validating strings @c
## Converting strings @c
## Summary @c
# 9) Building Structures @c
## Grouping in a structure @c
## Defining type structures @c
## Using Pointers in structures @c
## Pointing to structures @c
## Passing structures to functions @c 
## Grouping in a union @c
## Allocating memory @c
## Summary @c

# 10) Producing Results @c
## Creating a file @c
## Reading & writing characters @c
## Reading & writing lines @c
## Reading & writing entire files @c
## Scanning filestreams @c
## Reporting errors @c
## Getting the date and a time @c
## Running a timer @c
## Generating random numbers @c
## Displaying a dialog box @c
## Summary @c






