const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    const lib = b.addSharedLibrary(.{
        .name = "main",
        .root_source_file = .{ .path = "src/main.zig" },
        .optimize = .Debug,
        .target = .{ .cpu_arch = .wasm32, .os_tag = .freestanding },
        // .use_llvm = false,
        // .use_lld = false,
    });
    // export all functions marked with "export"
    lib.rdynamic = true;
    b.installArtifact(lib);
}
