You can create an executable or an library project,  Libraries can be either static or dynamic.  To create an executable use `zig init-exe`.  To create a library use `zig init-lib`.

Start in an empty directory

```bash
zig init-exe
```
This creates `build-zig` and `src`.  Your code belongs in the `src` directory.  The file `build.zig` directory contains directions on how to build the zig file.   We will go into this in depth in [[Build]].  This replaces tools like `make`, `cmake`, `autotools` or `cmake` and is implemented in the zig language.

## Build 
To compile and build the default project type:

`zig build`

This creates the following directories: `zig-cache` and `zig-out`.  Both of these files should be added to our .git ignore file.

The `zig-cache` directory contains temporary files that increase the compile speed.
The newly created program is at `zig-out/bin/hello`.  If we run it at the command line we see `hello`.

To build and run the program use `zig build run`.

## View Files

`vim src/hello.zig`
TODO: Flesh out the code

## Test
`zig build test`

`zig build test --summary all`

TODO: Fail test

## Git
Ignore any directory that starts with `zig`, which includes `zig-cache` and `zig-out`
.gitignore
```
zig-*
```
This is important for handling files across platforms.  This is important when working on files on Windows and also on Linux or macOS, since they use different carriage return characters.  Without this you will have lots of useless changes when transferring platforms.

.gitattributes
```
* test-auto
* *.zig text-auto eol=lf
```





