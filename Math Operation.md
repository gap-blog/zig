
%% Numeric Operation

```zig
_ = 345 *% 5; // Does Wrapping (modular math) 
+ = 345 *| 5; // Scoky diode (Saturation)
```

Math
```
+ - / * % +| -| /| *| 
```

Shifting 
```
<<
<<| (with Saturation)
>>
```

Bitwise operators
```
| & ^ ~ 
```

Casting
```
const byte: u8 = 200;
const word: u16 = 999;
var dword: u32 = byte + word;    // Safe cast
const w2: u16 = @intCast(dword); // Downcast
```

@intFromFloat(i);
@floatFromInt(f);
@addWithOverflow
@mulWithOverflow
@mod
@rem
@fabs
@sqrt
@min
@max
etc...

std.math // Numeric ops too
std.math.add, std.math.sub, std.math.divExact, etc...
