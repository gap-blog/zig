## Allocators

- Zig in Depth： Memory Management

1. What are allocators (https://www.openmymind.net/learning_zig/heap_memory/#allocators)
2. Why they are used
3. The heap
4. Types of allocators
5. How to use allocators
	1. `allocator.alloc`
	2. `try + defer free`
	3. `defer` and `errdefer`
	4. create and destroy (https://www.openmymind.net/learning_zig/heap_memory/#create)
6. Memory leaks and double frees
7. Types
	1. `std.heap.GeneralPurposeAllocator` (https://www.openmymind.net/learning_zig/heap_memory/#gpa)
	2. `std.testing.allocator` (https://www.openmymind.net/learning_zig/heap_memory/#testing)
	3. `std.heap.ArenaAllocator` (https://www.openmymind.net/learning_zig/heap_memory/#arena) (https://www.huy.rocks/everyday/01-12-2022-zig-how-arenaallocator-works)
	4. `std.heap.FixedBufferAllocator` (https://www.openmymind.net/learning_zig/heap_memory/#fixedbuffer)
	5. `std.heap.c_allocator`
	6. `std.heap.ThreadSafeFixedBufferAllocator`
	7. Custom (https://ziglang.org/documentation/master/#Implementing-an-Allocator)
		1. Look in std/heap.zig and `std.heap.GeneralPurposeAllocator`.

We can't forget about memory, which in Zig like C, must be handled manually.  There are many hazards in manual memory management in C, which has caused a slew of problems and venerability's that  hackers have tried and sometimes succeeded in exploiting.   Memory management is not something a programmer has to worry about in higher level languages like Javascript or Python so it might be unfamiliar to seasoned programmers.

There are 3 types of memory:
1. Stack
2. Heap
3. Program

### Stack Memory

Stack memory is the easiest to use and requires no special knowledge.  You use it when creating a local variable.  For example:

```zig
fn stackVariable() i8 { 
	const ret: i8 = 1;
	return ret + 1;
}
```

Here we are creating to instances of variables using stack memory.  The first is  _ret_ which is clearly defined.  The second is `ret + 1` which is a transitory variable and never named.  Stack memory is used in functions which are stacked on top of each other.  The variable `ret` must exist if `stackVariable()` calls a subroutine even though the subroutine can not access it.  This problem is solve by creating a stack of items in a function, only the top item of the stack can be accessed.  When the function is exited, by calling `return` or coming to the end of it, that function is removed or popped off the stack.

You may ask "Why is `ret + 1`" a stack variable?  In returning from a function a variable needs to be created or else you aren't returning anything.  When the function the returning values memory needs to be freed up just like any other variable.  If we aren't adding one to the return value we would just be returning `ret`, but `ret + 1` is clearly not `ret`.    

TODO: Explain the call stack better

### Heap Memory

Heap memory is used to create variables that last longer than a single function call.  Stack variables vanish when the function is done, heap variables don't.  The advantage of stack variables is they are easy to use.  The programmer doesn't need to worry about freeing them.  Programmers have the responsibility for managing them.  Here is an example of using heap memory.  We will discuss the actual syntax in a few moments.

```
fn heapMemory() !i8 {
	const ret = try allocator.alloc(i8, 4);
	errdefer allocator.free(ret);

	return ret;
}
```

TODO: explain above programs syntax

### Program Memory

The last type of memory is memory that exists in a program and can not change.  An example works best:

```
fn fourty_two() i8 {
	const ret: i8 = 42;
	return ret;
}
```
The function `fourty_two()` will always return that number.  While it is a useless function, the number 42 needs to exist somewhere before being put into ret.  This is program memory.  Now that you have learned about it you can forget about it because the compiler manages it completely.

### Problems managing heap memory

There are 3 kinds of problems programmers run into managing heap memory.  They are:
1. Not freeing memory
2. Over zealous freeing memory
3. Accessing memory that is not allocated

The first problem is the least harmful.  Not freeing memory will reduce the amount of memory your computer uses and can cause your computer to come to a grinding halt with the hard disk spinning furiously as you stare vacenlty at the beach ball of death.   Yikes!  That is bad, but the other two are worse.

Over zealous freeing of memory will cause the operating system to get confused about what memory is being used and isn't.  This can crash your program or your computer.  Bad, but we still have worse.

Accessing memory that is not allocated can cause create exploits that hackers or virus writers can use to take over your computer.  This is the worst case scenario.

Fortunately Zig has created some tools to help resolve these issues that don't exist in C.  They are:

1. Vectors has lengths.  That means we can be warned if we use not allocated memory
2. There is a testing allocator that will warn us if we forget to free memory or free memory too often.

Now for example programs to explore these issues and their resolutions.

TODO: Describe memory examples

Example of allocation and free
```zig
	var players = try allocator.alloc(Player, player_count);
	errdefer allocator.free(players);
```

The user has to handle memory manually, request it when it is needed and then tell the OS when they are done using it.  Memory is requested via an _allocator_.  There are several different types.  They are:

```zig
fn causeError() []8u {
	var array: [5]u8 = .{'H', 'e', 'l', 'l', 'o'};
	var s = array[2..];
	return s;
	// A slice is a pointer, so s is a pointer to array
}
```

```
// Caller must free returned value
fn works(allocator: std::mem.Allocator) std.mem.Allocator.Error![]u8 {
	var array: [5]u8 = .{'H', 'e', 'l', 'l', 'o'};
	const s = try allocator.alloc(u8, 5);
	// s is coherced into a slice
	std.mem.copy(u8, s, &array);
	return s;
}
```

Idioms
concat with caller allocating memory
fn concatSttatic(
	a: []const u8,
	b: []const u8,
	out: []u8,	
) usize {
	// Error if we do not have enough space
	std.debug.assert(out.len >= a.len + b.len);
	// Copy memory
	std..mem.copy(u8, out, a);
	std..mem.copy(u8, out[a.len.., b);
	// Return number of bytes copied
	return a.len + b.len;
	// Or returning the slice `) []u8 {`
	//return out[0 .. a.len + b.len);]
}

test "concatStatic" {
	const hello: []const u8 = "Hello ";
	const world: []const u8 = "world";
	var buffer[128]u8 = undefined;
	const len = concatSttatic(hello, world, &buffer);
	try std.testing.expectEqualString(hello ++ word, buf[0..len]);
}

With Allocation

// Takes an allocator, return concatinated string
// Caller must free result
fn catAlloc(
	allocator: std.mem.Allocator,
	a: [] const u8,
	b: [] const u8,
) ![]u8 {
	const ret = try allocator.alloc(u8, a.len + b.len);
	std.mem.copy(u8, out, a);
	std.mem.copy(u8, out[a.len ..], b);
	return bytes;
}

test "catAlloc" {
	const hello: []const u8 = "Hello ";
	const world: []const u8 = "world";
	const allocator = std.testing.allocator;
	const slice = try catAlloc(allocator, hello, world);
	try std.testing.expectEqualString(hello ++ world, slice);
	// allocator.free(slice);
}

With Failure problem

```
fn catAlloc(
	allocator: std.mem.Allocator,
	a: [] const u8,
	b: [] const u8,
) ![]u8 {
	const ret = try allocator.alloc(u8, a.len + b.len);
	std.mem.copy(u8, out, a);
	// errdefer allocator.free(ret)
	// This point may fail
	try mayFail();
	std.mem.copy(u8, out[a.len ..], b);
	return bytes;
}

test "catAlloc" {
	const hello: []const u8 = "Hello ";
	const world: []const u8 = "world";
	const allocator = std.testing.allocator;
	const slice = try catAlloc(allocator, hello, world);
	try std.testing.expectEqualString(hello ++ world, slice);
	// allocator.free(slice);
}

fn mayFail() !void {
	return error.Boom;
}

```

Structores that use memory
const Foo = struct {
	s: []u8,
fn init(allocator: std.mem.Allocator, s: [] sonst u8) !(Foo {
	const foo_ptr = try alloator.create(Foo);
	errdefer allocator.destory(foo_ptr);
	foo_ptr.s = try allocator.alloc(u8, s.len);
		std.mem.cpy(u8, foo_ptr.s, s);
	return foo_ptr;
})

fn deinit( self: *Foo, allocator: std.mem.Allocator) void {
	allocator.free(self.s);
	allocator.destory(self)	
}
}

test Foo {
	const allocator = std.testing.alloator;
		var foo_ptr = try Foo.init(allocator, greeting);
		defer foo_ptr.edinit(allocator);
		
		try std.testing.expectEqualString(greeting, foo_ptr.s);
}

Use allocator.create/allocator.destory with single elements
Use allocator.alloc/allocator.free with slices

