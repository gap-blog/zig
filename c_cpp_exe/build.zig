// build.zig
const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "hello-world",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const cppflags = [_][]const u8{"-std=c++17"};
    const cflags = [_][]const u8{""};

    // exe.linkLibC();
    exe.linkLibCpp();
    exe.addIncludePath(.{ .path = "src" });
    exe.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.cpp"), .flags = &cppflags });
    exe.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.c"), .flags = &cflags });

    b.installArtifact(exe);
}
