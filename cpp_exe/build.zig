// build.zig
const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    // const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "hello-world",
        .root_source_file = .{ .path = "src/main.zig" },
        // .target = target,
        .target = .{ .cpu_arch = .wasm32, .os_tag = .wasi },
        // Emscripten errors
        // .target = .{ .cpu_arch = .wasm32, .os_tag = .emscripten },
        // Freestanding errors
        // .target = .{ .cpu_arch = .wasm32, .os_tag = .freestanding }, 

        .optimize = optimize,
    });

    const cflags = [_][]const u8{"-std=c++20"};

    // exe.linkLibC();
    exe.linkLibCpp();
    exe.addIncludePath(.{ .path = "src" });
    exe.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.cpp"), .flags = &cflags });

    b.installArtifact(exe);
}
