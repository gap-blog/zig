## Forum
https://ziggit.dev/

[Communities](https://github.com/ziglang/zig/wiki/Community)

[Overview](https://www.openmymind.net/learning_zig/language_overview_1/)
[Crash Course](https://ikrima.dev/dev-notes/zig/zig-crash-course/)
[Another Overview](https://www.openmymind.net/learning_zig/language_overview_1/) I like this style of explanation
[Awesome Zig](https://github.com/C-BJ/awesome-zig)
[Cookbook](https://zigcc.github.io/zig-cookbook/)
## Ziggit
https://ziggit.dev/top?period=monthly
username: gap
password: `g**22g**`

Can compile to [[Wasm]]

[Cloudflare Workers](https://github.com/CraigglesO/workers-zig)

[Zig Learn](https://ziglang.org/learn/)
[Getting Started: Log Rocket](https://blog.logrocket.com/getting-started-zig-programming-language/)

[Zig and webassmebly](https://dev.to/sleibrock/webassembly-with-zig-part-1-4onm) Includes how to call js from zig
[Zig examples with Webassbley](https://github.com/sleibrock/zigtoys)
[Build a cpp file](https://ziggit.dev/t/how-to-use-build-zig-to-build-c-code/2080)

[Zig month newsletter](https://zig.news/)

Zig async https://github.com/ziglang/zig/issues/6025#issuecomment-1914725896

Videos
[The Road to Zig 1.0](https://www.youtube.com/watch?v=Gv2I7qTux7g) Talk about movitvation from the author

[Ziglings: Exercises for learning Zig](https://codeberg.org/ziglings/exercises/)


