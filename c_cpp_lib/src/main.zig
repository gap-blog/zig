const c = @cImport({
    @cInclude("bindings.h");
});

pub fn main() !void {
    _doSomeCppThing();
    doSomeCThing();
}

pub fn doSomeCThing() void {
    c.doSomeCThing();
}

export fn _doSomeCppThing() void {
    c.doSomeCppThing();
}
