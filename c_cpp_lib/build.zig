// build.zig
const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const lib = b.addSharedLibrary(.{
        .name = "test",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    lib.linkLibC();
    lib.linkLibCpp();

    lib.addIncludePath(.{ .path = "src" });

    const cflags = [_][]const u8{""};
    lib.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.c"), .flags = &cflags });
    const cppflags = [_][]const u8{"-std=c++17"};
    lib.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.cpp"), .flags = &cppflags });

    lib.export_symbol_names = &.{ "cpp_function", "_doSomeCppThing" };
    lib.export_symbol_names = &.{ "c_function", "doSomeCThing" };
    b.installArtifact(lib);
}
