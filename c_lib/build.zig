const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    const cflags = [_][]const u8{""};

    const lib = b.addSharedLibrary(.{
        .name = "test",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = .{},
        .optimize = .ReleaseSmall,
    });

    lib.linkLibC();
    // lib.linkLibCpp();

    lib.addIncludePath(.{ .path = "src" });
    lib.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.c"), .flags = &cflags });

    lib.export_symbol_names = &.{ "add", "cpp_function" };
    b.installArtifact(lib);
}
