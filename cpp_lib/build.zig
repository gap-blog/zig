// build.zig
const std = @import("std");

pub fn build(b: *std.build.Builder) void {
    const lib = b.addSharedLibrary(.{
        .name = "test",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = .{},
        .optimize = .ReleaseSmall,
    });

    //  lib.linkLibC();
    lib.linkLibCpp();

    lib.addIncludePath(.{ .path = "src" });
    const cflags = [_][]const u8{"-std=c++17"};
    lib.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.cpp"), .flags = &cflags });

    lib.export_symbol_names = &.{ "add", "cpp_function" };
    b.installArtifact(lib);
}
