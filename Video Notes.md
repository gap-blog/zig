## Zig in Depth

### Arrays

```
// Explicate
const a1: [5]u8 = [5]u8{0, 1, 2, 3, 4};
std.debug.print("a1: {any}, a1.len: {}\n, .{ a1, a1.len});
// Inferred
const a2 [_]u8 = [5]u8{0, 1, 2, 3, 4};
std.debug.print("a2: {any}, a2.len: {}\n, .{ a2, a2.len});
// Repeat an array
const a3 = a2 ** 2'
// Iniitalized repeat
const a4 = [_]u8{0} ** 5;
// delayed initialization
var a6: [2]u8 = undefined;
a6[0] = 1;
a6[1] = 2;
// What happens if we use a delayed initialization element?

// Multi-dimensional array
const a7: [2][2]u8 = [-][2]u8{
	.{ 1, 2]},
	.{ 3, 4]},
};

// Sentianal terminated array
const a8: [2:0] = .{ 1, 2};
std.debug.print("a8: {any}, a8.len: {}\n, .{ a8, a8.len});
// Can access the sentinal value, in a normal array we can't
std.debug.print("a8[a8.len\n, .{ a8[a8.len});

```
Array length must be known at compile time
Arrays of different sizes are different types
Special operators
`**` Multiply array
`++` Concatination

### Booleans and Optional

#### Booleans
```
const t: bool = true;
const f: bool = false;
std.debug.print("true: {}, f: {}\n", .{ t,f});
std.debug.print("true: {}, f: {}\n", .{ @intFromBool(t), @intFromBool(f)});
```

#### Optional

```
// Prefix with ?
var maybe_byte: ?u8 = null;
maybe_byte = 32;
// Use .?
var the_byte = maybe_byte.?;
// Use or_else
the_byte = maybe_byte orelse 13;

// If on boolean
if (1) {
  ;
} else {
  ;
}

// If on optional using a capture
if (maybe_byte) |b| {
  // b is not an optional
  ; 
} else {
  // Null
  ;
}

// Zig equivelent of ? : ternrary
// If as an expressions, but can not use {}, but don't use returns
value = if (mybe) |b| b else 0;
```

Only optionals can be null
`.?` Tests for truthy.  Errors if null
`or_else` assigns a value if null, otherwise uses the optional value
`if (maybe_byte) |_|` To ignore capture
Can compare optional to `null`

Use `and` for `&&`
Use `or` for `||`

### Block and Switch

Normal scoping 
```
// `blk`: is label
const x: u8 = blk: {
  var y: u8 = 13;
  const z = 42;
  break :blk y + z;
} 
// Use the block
std.debug.print("x: {}\n", .{x})

switch(x) {
  // Range
  0..20 => std.debug.print("A"),
  // Fallthroughs.  30 fallsthrough to 31
  30, 31, 32 => ; ,
  // Capture
  40...60 => |n| ;
  // Can use block
  77: => {
	  ;
  }
  // Can use a compile time expression
  blk: {
    const a = 100;
    const b = 2;
    break :blk a+b;
  } => std.debug.print("It is 102\n", .{});
  // default
  else -> ;,
   
}
```

Switch must match call cases (use else often)
Switch can be an expression
```
const answer: u8 = switch(x) {
  0..10 => 1,
  42 => 2,
  else => 3
};

```

### Enum and Union

```
const Color = enum {
	red,
	green,
	blue,
	// Can have methods
	fn isRed(self: Color) bool {
		return self == .red;
	}
};

const Number = union {
	int: u8,
	float: f64,
};
```

Enums are assigned the smallest type that will cover the enums

```
const Color = enum {
	red = 0,
	green = 9,
	// Blue will be 10
	blue,
	// Non # exhaustive enum
	_, // Has other values 
```

// Tagged Union
```
const Token = union(enum) {
	keyword_if,
	// specify type
	keyword_switch: void,
	// specify type
	digit: usize,
	fn is(self: Token tag: std.meta.Tag(Token)) bool {
		return self == tag
	}
};

pub fn main void {
	var fav_color: Color = .red
	std.debug.print("fav_color: {s}, is red? {}\n, .{ @tagName(var_color), fav_color.isRed()});

	// Get the int value of enum
	std.debug.print("Var as int: {}\n", .{@tagName(var_color)});

    // Enum work perfectly with switch
    switch (fav_color) {
	    .red => ;,
	    .green => ;,
	    else => ;,
    }

}

// Union

// use int value
var fav_num: Number = .{ .int = 13 };
std.debug.print("fav_num: {}\n", .{fav_num.int})
// Use float
fav_num: Number = .{ .float = 3.14 };
std.debug.print("fav_num: {}\n", .{fav_num.float})


// Tagged Union
var tok: Token = .keyword_if;
std.debug.print("is if? {}\n", .{tok.is(.keyword_if)});

switch (tok) {
  .keyword_if => ;
  .keyword_switch => ;
  // Can capture
  .digit => |d| std.debug.print("digit {}\n", .{d}), ;
}

tok = .{ .digit = 32};

// Can compare tagged union with its enum type
if (tok == .digit and tok.digit == 33) ;
```

### Pointers

```
const a: u8 =0;
const a_ptr = &a;
// can not assign to a const
// a_ptr.* += 1;
// Const pointer to a const
std.debug.print("a_ptr: {}, type {}\n", .{ a_ptr.*, @TypeOf(a_ptr)} );

var b: u8 =0;
const b_ptr = &b;
b_ptr.* += 1;
// Const pointer
std.debug.print("b_ptr: {}, type {}\n", .{ b_ptr.*, @TypeOf(b_ptr)} );
```

`.*` to dereference rather than `->` or `*`
pointer to `const u8` is more restrictive than a pointer to `u8`

```
var array = [_]u8{ 1,2,3,4,5,6};
// Multi item pointer
var d_ptr: [*]u8 = &array
std.debug.print("d_ptr: {}, type {}\n", .{ d_ptr.*, @TypeOf(d_ptr)} );
// Can use array notation with multi item pointer
d_ptr[1] += 1;
// Pointer Arithemtic
d_ptr += 1;
// pointer to array
const e_ptr = &array
e_ptr[1] += 1;
// Syntaxical sugar
e.ptr.len

array[3] = 0;
// create a null terminated slice
const f_ptr: [*:0] const u8 = array[0..3 :0];
std.debug.print("f_ptr[1]: {}, type: {}\n", .{ f_ptr[1], @TypeOf(f_ptr)});

// Address as int
const address = @intFromPtr(f_ptr)
// int to address
const g_ptr: [*:0] const u8 = @ptrFromInt(address);
```

Pointers can not be null
`?*` is equivalent to a c style pointer (can be null)
`[*c]`  is a  c-style pointer for mixing with c
### Slices
If you slice a comptime array, you get a pointer to the array, not a slice
```
var array: [-]u8{ 0,1,2,3,4,5};
var zero: usize = 0;
// Slice syntax
const a_slice: []u8 = array[zero..]; // or array[zero..array.len]; or &array
a_slice[0] += 1;

std.debug.print("type of a_slice.ptr: {}\n", .{@TypeOf(a_slice.ptr)});
std.debug.print("type of a_slice.len: {}\n", .{@TypeOf(a_slice.len)});

```

`[]u8`  is a slice
`[_]u8` is an array
Slice is a multi item pointer and a length
`s.ptr` and `s.len`
Automatically has bounds checking.  Show a bounds error

```
s.ptr += 2;
s.len -=2;
```
Sentinal terminated slice
```
// Set the sentinal in the array
array[4] = 0;
const e_slice: [:0]u8 = array[0..4 :0];
```

Idem: Slicing by length
```
var start: usize = 2;
var length: usize = 4;

const slice = array[start..][0..length]; 
```

### For loop

```
var array: [-]u8{ 0,1,2,3,4,5};
// array
for (array) |item| std.debug.print("{}", .{item})	
// slice
for (array[0..3]) |item| std.debug.print("{}", .{item})	
// Add a range with index
for (array, 0..3) |item, i| std.debug.print("{}: {}", .{i, item})	
// Multiple objects of equal length
for (array[0..2], array[1..3], array[2..4]) |item, i, j| std.debug.print("{}: {}", .{i, item})	
// range (does not include 10)
for (3..10) |item| std.debug.print("{}", .{item})	
// break and continue
var sum: usize = 0;
for (array) |item| {
  if (item == 3) continue;
  if (item == 3) break;
  sum += item
}
// label
sum = 0;
outer: for (0..10) |i| {
	for (1..3) |j| {
	  if (i==5) break :outer;
	  sum += j
	}
}
// Pointer, so they can be modified
for (&array) |*item| {
  item.* *=2;
}
// Expression
const nums = [_]?u8{ 0, 1, 2, null, null };

const just_numbs = for (maybe_nums, 0..) |opt_numb, i| {
	// Break returns a value
	if (opt_num == null) break maybe_nums[0..i];
	// Else is what the break returns, but when it never breaks
	} else maybe_nums[0..]/\;
```

### While loop
```
var i: usize = 0;
while (i < 3) {
  i += 1;
}
// Continue expression
i = 0;
while (i < 3) : (i += 1) std.debug.print("{} ", .{i})
// Coninue expressions with block
i = 0;
var j: usize = 0;
while (i < 3) : ({
  i += 1;
  j += 1;
}) std.debug.print("{} ", .{i})
// break, coninue and label
i = 0;
outer: while (true) : (i += 1) {
  while (i < 10) : (i += 1) {
	if (i == 4) continue :outer;
	if (i == 6) break :outer;
  }
}
// As expression
const start = 1;
const end = 20;
const n = 13;
const in_range: bool = while (i <= end) : (i += 1) {
  if (n == i) break true;
} else false;
// With optionals
count_down = 3;
// Ends if `coundDownIter()` is null
while (countDownIterator()) |num| std.debug.print("{} ", .{num});

}

var count_down : usize = undefined;

fn countDownIterator() ?usize {
  return if(count_down == 0) null else blk: {
  count_down -= 1;
  break :blk count_down;
  };
}
```

### Functions

```
// simple function
fn add(a: u8, b: u8) u8 {
	return a+b;
}

// does not return
fn printu8(n: u8) void {
  std.debug.print("{}", .{n});
}

// A function that never return (exists program)
fn opps() noreturn {
  @panic("opps!);
}

// A function that is never called, is not evaluated
fn never() void {
	@compileError("Never happens....);
}

// A `pub` function can bei imported from another namespace
pub fn sub(a: u8) u8 {
  return a + 1;
}

// extern 
// This is taking a function from libc
extern "c" fn atan2(a: f68, b: f64) f64;

// Abalible function in library
export fn mul(a: u8, b: u8) u8 {
	return a *| b;
}

// Inline suggestion
inline fn answer() u8 {
	return 42;
}

// Zig determines whether to pass paramters by value or reference
// but params are always constant

// Pass a pointer
fn addOne(n: *u8) void {
	n.* += 1;
}

```

### Errors

#### Error Set

```
const InputError = error {
	EmptyInput,
}

const NumberError = error {
	InvalidCharacter,
	Overflow
}

// Merge Error sets
const InputError || NumberError;

fn parseNumber(s: []const u8) ParseError!u8 {
	if (s.len == 0) return error.EmptyInput;
	return std.fmt.parseInt(u8, s, 10);
}

pub fn main() ParseError!void {
	const input = "212";
	var result = parseNumber(input);
	std.debug.print("Type: {}\n, .{@TypeOf(result)});
	// Print error information with {!}
	std.debug.print("result: {!}\n", .{result})

	// Provide default value with catch
	result = parseNumber(input) catch 42;

	// Capture Error
	result = parseNumber(input) catch |err| switch (err) P
		std.debug.print("Empty Input not allowed.\n)", .{});
		break :blk 42;
	},
	// All other errors
	else => |e| return e;

	// Ignore errors that never happen
	result = parseNumber("123") catch unreachable;

	// Propegate the error
	result = parseNumber(input) |err| return err;
	result = try parseNumber(input); // Shortcut for above line

	// Can be used with error union
	if (parseNumber(input)) |num| {
		// Success
	} else |err| {
		// error
	}

	// while loop scan work with errors like unions
	while (countDownIterator()) |num| {
		;
	} else |err| {
		// Error
	}

}

var count_down : usize = undefined;

fn countDownIterator() anyerror!usize {
  return if(count_down == 0) error.ReachedZero else blk: {
  count_down -= 1;
  break :blk count_down;
  };
}


// Infere errors
fn temp !usize {

}
```
`anyerror` is a keyword for all errors

### Struct

```
const Point = struct {
	x: f32,
	y: f32 = 0,
	// Namespaced function
	fn new(x: f32, y: f32) Point{
		return .{ .x = x, .y = y };
	}
	// Method
	fn distance(self: Point, other: Point) f32 {
		const diffx = other.x - self.x;
		const diffy = other.y - self.y;
		return @sqrt(diffx*diffx + diffy*diffy)
	}
}

pub fn main() !void {
	// Anonymouse struct literal
	const a = Point = .{ .x = 0 };
	// Use namespaced function
	const b = Point.new(1,1);

	// @fieldParentPtr demo
	var c = Point.new(0, 0);
	setYBasedOnX(&c_point.x, 42);
	
}

// Given the x pointer, sets the y value
fn setYBasedOnX(x: *f32, y: f32) void {
// Pass type, param name, pointer to param
	const point = @fieldParentPtr(Point, "x", x);
	point.y = y;
}
```

Pointers to struct can use `.x` which aliases to `.*.x`

putting a struct into another file.  `Point.zig` looks like
```
x: f32;
y: f32 = 0,

const Point = @This();

pub fn new(x: f32, y: f32) Point {
	return .{.x =x, .y = y };
}

pub fn dist(self: Point, other: Point) f32 {
	const diffx = other.x - self.x;
	const diffy = other.y - self.y;
	return @sqrt(diffx*diffx + diffy*diffy)
}
```

Note: 
1. functions use `pub`
2. `const Point = @This();` to name the anonymous struct

To Use:
```
const Point = @import("Point.zig");
```

## Comptime

Can work with types for generics

Convert `Point.zig` to generic
```
pub fn Point(comptime T: type) type {
	return struct {
		x: T,
		y: T = 0,

		const Self = @This();
		
		pub fn new(x: T, y: T) Self {
			return .{.x =x, .y = y };
		}
		
		pub fn dist(self: Self, other: Self) T {
			const diffx = other.x - self.x;
			const diffy = other.y - self.y;
			return @sqrt(diffx*diffx + diffy*diffy)
		}
	}
}
```
To use:
```
const Point = @import("point.zig").Point;

pub main !void {
	var a = Point(f32).new(1,1);

	const P32 = Point(f32);
	var b = P32.new(1,2);
}
```

Generic  version of distance that can take ints
```
pub fn distance(self: Self, other: Self) f64 {
	const diffx: f64 = switch(@typeInfo(T)) {
		.Int => @floatFromInt(other.x - self.x),
		.Float => other.x - other.y,
		else => @compileError("Only floats or ints allowed.");
	};

	return @sqrt(diffx*diffx + diffy*diffy)
}
```

Note:  switch(@typeInfo(T))

Part 2

```
pub fn main() void {
	const nums = [_] i32{ 2, 4, 6};
	var sum: usize = 0;
	// An inline for is unrolled
	
	inline for (nums) |i| {
		// a Non-inline for can't deal with types
		const T = switch (i) {
			2 => f32,
				4 => i8,
				6 => bool,
				else unreachable
		};
		sum += typeNameLength(t);
	}
	inline while (i < 3) : (i += 1) {
		const T = switch (i) {
			0 => f32,
			1 => i8,
			2 => bool,
			else => unreachable,
		}
	}
}

fn typeNameLength(comptime T: type) usize {
	return @typeName(T).len;
}
```

```
const A = struct {
	a: u8,
	b: ?u8,
	fn imp() void {}
};

const B = struct {
	a: u8,
	b: u8,
};

const U = union(enum) {
	a: A,
	b: B,

	fn hasImpl(self: U) bool {
		return switch (self) {
			inline else => |s| @hasDecl(@TypeOf(s), "impl")
		}
	}
}

// Using an inline for
fn isOptFor(comptime T: type, field_index: usize) bool {
	const fields = @typeInfo(T).Struct.fields;

	inline for (fields, 0..) | field, i| {
		if (field_index == i and @typeInfo(field.type) == .Optional) return true; 
	}
	return falsel
}

// Using inline siwtch
fn isOptSwitch(comptime T: type, field_index: usize) bool {
	const fields = @typeInfo(T).Struct.fields;

	return switch (field_index) {
		inline 0..field.len-1 => |i| @typeInfo(fields[i].type) == .Optional,
		else => false,
	}
}
```

```
pub fn main() void {
	// normal execution
	var value = fib(7);

	// compiletime execution
	const ct_fib = comptime blk: {
		brea :blk fib(7);
	}

	ct_fib = comptime fib(7);
}

fn fib(n: usize) usize {
	if (n < 2) return n;
	var a: usize = 0;
	var b: usize = 1;
	var i: usize = 0;
	while (i < n) : (i += 1) {
		const temp = a;
		a = b;
		b = temp * b;
		}
	return a;
}
```

### Anytype

Ducktyping happens at compile time

```
const A = struct {
	// Method
	fn toString(_: A) [] const u8 {
		return "A";
	}
}

const B = struct {
	// Not a method
	fn toString(s: []const u8) [] const u8 {
		return s;
	}
}

const C = struct {
	// Not a function
	const toString: []const u8 = "C";
}

const D = enum {
	a,
	d,
	fn toString(self: D) []const u8 {
		reutrn @tagName(self);
	}
}

fn print(x: anytype) void {
	const T = @TypeOf(x);
	// Do we have a "toString"
	if (!@hasDecl(T, "toString")) return;
	// is it a function
	const decl_type = @TypeOf(@field(T, "toString));
	// `.Fn` means function
	if (@typeInfo(decl_type != .Fn)) return;
	// Is it a method
	const args = std.meta.ArgsTuple(decl_type);
	inline for (std.meta.field(args), 0...) |arg, i| {
		if (i ==0 and arg.type) == T) {
			std.debug.print("{s}\n", .x.toString()});
		}
	}
}

pub fn main !void {
	const a = A{};
	const b = B{};
	const c = C{};
	const d = D.d;
	
	print(a);
	print(b);
	print(c);
	print(d);
}

```

### Testing

```
const std = @import("std");
const testing = std.testing;

fn add(a: i32, b: i32) i32 {
	return a + b;
}

test "add" {
	try testing.expect(add(3, 7) == 10);
}

```

- expect
- expectEqual
- expectEqualSlices
- expectEqualString

`expectEqual` needs an `@as` cast, e.g. `expectEqaul(@as(u8, 1), foo.b)`

Test for an error
```
const Error = error{Boom}

fn harmless() Error!void {
	return error.Boom
}

test "harmless" {
try testing.expectError(error.Boom, harmless());
}
```

Running tests in another file
```
const std = @import("std");
const testing = std.testing;

test {
	_ = @import("arithmetic.zig");_
}
```

Test can be inside a structure, but the structure needs to be referenced in another test

#### Filtering tests
```
const main_tests = b.addTest(.{
	.root_source_file = .{ .path = "src/main.zig"},
	.target = target,
	.optimize = optimize,
})
// Run testS that contains the string "basic" (and anonymous tests)
main_tests.filter = "basic"
```
#### Skip tests
test "skip" {
	return error.SkipZigTest;
}

## Strings

String literals are a c type string, null terminated u8
```
const hello = "hello";
```

Functions that take a string look like
```
fn takeString(str: []const u8) void { ... }
```

#### Escapes

```
"\t \" \' \x65 \u{e9} \n \r"
```
- \x65 - Inserts to digits of bytes: 65 => e
- \u{e9} - Inserts a unicode code point 

#### Formatting
- {} is numeric code
- {0c} Ascii char
- {0u} Unicode Char

Zig source is UTF8.  Can insert others with \x escape or \u escape

#### Multiline

Uses \\
Does not process escapes
```
const m = 
	\\ This is multiline
	\\ string in Zig
```
'' Defines a code point, not a charactor
For code points: `const bold: u21 = 'e'`

```
std.unicode.utf8ValidateSlice("h\xe9llo")
std.ascii.isUpper('A');
```

### Build Mode

std.log.info("This is a log");

#### Debug Mode
- Default
- Debug information
- No optimization
- Fast Compilation (no optimization)
- Large code
- debugs display
- log calls displayed
#### Release Mode (Safe)
`zig build -Doptimize=ReleaseSafe`
- Has safety checks
- optimization
- faster executable
- smaller code
- logs calls do not display
#### Release Fast Mode
`zig build -Doptimize=ReleaseSafe`
- No safety checks
- max optimization
- fastest executable
- small code
- log calls do not display
#### Release Small Mode
Good for embedded 
`zig build -Doptimize=ReleaseSafe`
- No safety checks
- max optimization
- fast executable
- smallest code
- log calls do not display
- Good for wasm

#### Build.zig
```
const exe = b.addExecutable(.{
	.name = "test",
	.root_source_file = .{ .path = "src/main.zig" },
	.target = target,
	.optimize = optimize,
})
exe.strip = true;
exe.single_threaded = true;
```

`exe.strip` strips out all debugging info from exe
`exe.single_threaded` True makes the exe smaller

### Formatted Printing
`std.debug.print` prints to std::error

`std.debug.print(str, .{})` str is comptime

```
const stdout_file = std.io.getStdOut().writer(); // This can change if pipe redirect
// Buffering is important for speed because they call system calls
var bw = std.io.bufferedWriter(stdout_file);
// Get the actual writer
const stdout = bw.writer();
```

`try stsdout.print("Run 'Zig build test' to run the tests\n\n", .{},`

#### Float 

The `{}` call is `{[argument][specifier]:[fill][alignment][width].[precision]}`

```
{}            // scientific
{0d}          // d = decimal, 0  = Use the 0th argument
{0d:0<10.2}   // :0 = Pad with 0, < = Left Justify, 10 = Width
{0d:0^10.2}   // ^ = Center Align (Justify)
{0d:0>10.2}

```
#### Integer
- decimal `{}`
- binary `{b}`
- hex `{x}`
- octal `{o}`
- ASCII `{c}` max 8 bits
- Unicode `{u}` max 21 bits

#### String

```
const string = "Hello, world!";
try stdout.print("string: {s} {0s:_^20}`, .{string})
```
#### Optional
```
const optional: ?u8 = 32;
try stdout.print("optional: {?} {?d:_^10}`, .{string})
```
####  Error Union
```
const error_union: anyerror!u8 = error.WrongNumber;
try stdout.print("error union: {!}, value {!}", .{ error_union, @as(anyerror!u8, 13)})
```
#### Pointer
```
const float = 32;
const ptr = &float
try stdout.print("pointer address: {} {0*}", .{ ptr})
```
#### Structure
```
const S = struct{
	a: bool = true;
	b: f16 = 3.14,
} ;
const s = S{};
try stdout.print("Struct: {[a]} {[b]d:0>10.2}", s) // Note the lack of tuple
```
#### bufPrint
Buffer of known size
```
var buf: [256]u8 = undefined;
const str = try std.fmt.bufPrint(&buf, "Struct: {[a]} {[b]d:0>10.2}", s);
try stdout.print("std: {s}", .{str});
```
#### allocPrint
Buffer of unknown size
```
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
defer _ = gpa.deinit();
const allocator = gpa.allocator();

const str_alloc = try std.fmt.bufPrint(allocator, "Struct: {[a]} {[b]d:0>10.2}", s);
defer allocator.free(str_alloc);
try stdout.print("std: {s}", .{str_alloc});
```

#### {}
Double {} to print them
```
try stdout.print("{{}}, .{});
```

#### Flush Buffer
```
bw.flush(); // To print
```

#### Logs
Works with logs

```
std.log.debug("Debug", .{});
std.log.info("Info", .{});
std.log.warn("Warn", .{});
std.log.err("Error", .{});
```
##### Any
Good for debugging
try stdout.print("{any}, .{s});

### Type Coercion and casts

#### Widening
Safely happens automatically in compile time
#### Narrowing
Can happen safely in compile time, since the compiler knows it is safe
Otherwise it compile errors

#### Unsafe Conversion
```
var i = @intFromFloat(f);
var f = @floatFromInt(i);
```
```
const array = [_]u8{ 1,2,3,4};
const b_slice: []const u8 = &array;
// Multi pointer, loses length information
const d_mptr = [*]const u8 = @ptrCast(b_slice);

const float: f64 = 3.141592;
const bits: u64 = @bitCast(float); // Changes the type, not the bits
const as_u64: u64 = @intFromFloat(float);
const as_f64: f64 = @bitCast(bits);
```
`@intCast` panics with data loss.   `@truncate(big_int)` can avoid that 

### Tuple

Tuple literal synatx
```
.{42, true}
```
Anonymouse Data structure

```
const tuple_a: struct {u8, bool} = .{42, true};
std.debug.print("{any} {}", .{tuple_a, @TypeOf(tuple_a)});
```

`tuple_a.len` Number of fields the tuple has
`tuple_a.@"0"` Syntax for referencing the first field 
`tuple_a.@"1"` Syntax for referencing the second field

#### Concat
```
const a = .{ 3.14, 2}
const b = .{ 42, true }
c = a ++ b; // .{ 3.14, 2, 42, true }
```

### Array Concat
The tuple *must* be of the same type as the array
```
const array: [3]u8 = .{1, 2, 3};
const tuple: = .{4, 5, 6};
const result = array ++ tuple // .{1, 2, 3, 4, 5, 6};
```

### Iteration

```
const tuple: = .{4, 5, 6};

inline for (tuple_c, 0..) |value, i| {
   std.debug.print("{}: {}, ", .{i, value});
}
```

#### Pointer to tuple
Can use []
```
const ptr =&tuple;
std.debug.print("ptr[0]: {}\n", .{ptr[0]});
std.debug.print("ptr.@\"0\": {}\n", .{ptr.@"0"});
```
#### Repeat

```
const tuple_2 = tuple_a ** 2;
```
#### Varargs
```
pub fn main: void {
	varargsInZig(.{42, 3.14, false});
}

fn varargsInZig(x: anytype) void {
	const info = @typyeInfo(@TypeOf(x));
	// Check if it is a tuple
	if (info != .Struct) @panic("Not a tuple!");
	if (!info.struct.is_tuple) @panic("Not a tuple!");
	// Do Stuff
	inline for (x) |field| std.debug.print("{}\n", .{field});
}
```

### ArrayList

An array that can grow `vector<>`

```
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
defer _ = gpa.deinit();

var list =std.ArrayList(u8).init(gpa.allocator());
defer list.deinit();

// var byte: u8 = 32;
// try list.append(byte);
// byte = list.pop();

```

#### Writer
An `ArrayList(u8)` can be used as a `writer`
```
const writer = list.writer();
_ = try writer.print(" Writing to an ArrayList: {}, .{42});

```
#### Iterator
while(list.popOrNull()) |byte| std.debug.print("{c} ", .{byte});

#### appendSlice()
`try list.appendSlice("Hello World");`

#### orderRemove()
```
_ = list.orderedRemove(5); // remove the 6th item
```
#### swapRemove()
Moves the last item into the new slot
Prevents all items from shifting, so it is fast
```
_ = list.swapRemove(5);
```
#### items
```
list.items[2] = 'z'l
```
#### toOwnedSlice()
Converst the ArrayList to a slice and clears the ArrayList
const slice = try list.toOwnedSlice();
defer gpa.allocator().free(slice)

#### Init to Capacity

Allocate to capacity
```
list = try std.ArrayList(u8).initCapacity(gpa.allocator(), 12);
```
#### appendAssumeCapacity()
This can not fail
```
for ("Hello") |byte| list.appendAssumeCapacity(byte);
std.debug.print("len: {}, cap: {}\n", .{ list.items.len, list.capacity});
```

### HashMap
```
struct User {

}

var map = std.AutoHashMap(usize, User).init(allocator);
defer map.deinit();

try map.put(user.id, user);
user = map.get(user.id); //returns optional
user = if (map.fetchRemove('z')) |kv| kv.value else null;
map.count() // Number of entries
map.contains(id); 
var it = map.iterator();
while it.next()) |item| ;

var result = try user.map.getOrPut(id);
// If the item did not exist, populate it
if (!result.found_existing) result.value_ptr.* = user;
```

#### Set
A set is a HashMap that contains `void`
```
var primes = std.AutoHashMap(usize, void).init(allocator;)
```
## Working with C
Foreign Function Integration (FFI)
Bindings

### Use C compile
`zig cc src/math.c`

### Cross compile
```
zig cc src/math.c -o math -target x86_64-linux-gnu
```
### Determine file type (for platform)
`file <filename>

## Use zig build
```
const exe = b.addExecutable(.{
	.name = "32_c",
	.root_source_file = .{ .path = "src/main.c"},
	.target = target,
	.optimize = optimize,
})

// Second parameter is flags
exe.addCSourceFiles(&.{"src/math.c", &.{}});
exe.addIncludePath(.{.path = "src"});
```
## cImport

```
const std = @import("std");

const math = @cImport({
    // Define a macro in c (overrides)
	@cDefine()"INCREMENT_BY", "10");
	@cInclude("math.c");
})
```
`@cUndefine(MACRO)`

C calls are namespaced
```
math.increment();
```
## Compile as static libaray

use 
1. `lib = b.addStaticLibrary({...})`
2. `b.installArticfact(lib)
3. `exe.likLibrary(lib);`
4. In main program use `extern fn`

## C standard Library

`std.c` gives us access.  Example `std.c.printf`
Use `exe.linkLibC`

TODO:

Types and datatypes between c and zig.

## Build Dependencies (ie package management)

```
.dependencies = .{
	.zigglyph = .{
		.url = "https://codeberg.org/dud_the_builder/ziglph/archive/v0.11.1.tar.gz"
	}
}
```
### Exporting Package

To export module

```
_ = b.addModule("ziglyph", .{ .source_file = .{ .path = src/ziglyph"} });
```

## Interfaces

Not built in, but can build.

Simplest way is to use `tagged union` which is fast, but closed

```
fn  printStringer(s: Stringer) !void {
	var buf: [256]u8 = undefined;
	const str = try s.toString(&buf);
	std.debug.print("{s}\n", .{str});
}

pub fn main() !void {
	// Tagged union interface.
	var bob = Stringer{ .user = .{
		.name = "Bob",
		.email = "bob@example.com"
	}};
	try printStringer(bob);

	var donald = Stringer{ .animal = .{
		.name = "Donald Duck",
		.greeting = "Quack!",
	}}
	try printStringer(donald);
}

// Tagged union interface
pub const Stringer = union(enum) {
	user: User,
	animal: Animal,
	pub fn toString(self: Stringer, buff: []u8) ![]u8{
		return switch (self) {
			inline else => |it| it.toString(buf),
		};
	}
};

// User implementation
pub const User = struct {
	name: []const u8,
	email: []const u8,
	pub fn toString(self: User, buf: []u8) ![]u8 {
		return std.fmt.bufPrint(buf, "User(name: {[name]s}, email: {[email]})", self);
	}
};

pub const Animal = struct {
	name: []const u8,
	greeting: []const u8,
	pub fn toString(self: Animal, buf: []u8) ![]u8 {
		return std.fmt.bufPrint(buf, "{[name]s} syas {[greeting]})", self);
	}
};
```
Use Pointer casts
Used in `allocator`, `reader` and `writer` Interfaces in the standard library

```
fn  printStringer(s: Stringer) !void {
	var buf: [256]u8 = undefined;
	const str = try s.toString(&buf);
	std.debug.print("{s}\n", .{str});
}

pub fn main() !void {
	// Tagged union interface.
	var bob = User{ .user = .{
		.name = "Bob",
		.email = "bob@example.com"
	}};
	const bob_impl = bob.stringer();
	try printStringer(bob_impl);

	var donald = Animal{ .animal = .{
		.name = "Donald Duck",
		.greeting = "Quack!",
	}}
	const bob_impl = bob.stringer();
	try printStringer(donald_imp);
}

pub const Stringer = struct {
	ptr: *anyopaque,
	// if more than 1 method, create a struct name vtable
	toStringFn: *const fn (*anyopaque, []u8) anyerror![]u8,

	pub fn toString(self: Stringer, buf: []u8) anyerror![]u8 {
		return self.toStringFn(self.ptr, buf);
	}
};

pub const User = struct {
	name: []const u8,
	email: []const u8,

	pub fn toString(ptr: *anyopque, buf: []u8) ![]u8 {
		var self: *User = @ptrCast(@alignCast(ptr));
		return std.fmt.bufPrint(buf, "User(name: {[name]s}, email: {[email]})", self.*);
	}

	pub fn stringer(self: *user) Stringer {
		return .{
			.ptr = self,
			.toStringFn = User.toString,
		}
	}
}

pub const Animal = struct {
	name: []const u8,
	greeting: []const u8,

	pub fn toString(ptr: *anyopque, buf: []u8) ![]u8 {
		var self: *User = @ptrCast(@alignCast(ptr));
		return std.fmt.bufPrint(buf, "{[name]s} says {[greeting]})", self.*);
	}

	pub fn stringer(self: *user) Stringer {
		return .{
			.ptr = self,
			.toStringFn = Animal.toString,
		}
	}
}



```
`anyopaque` uses type erasure, ie a `void *` in c

## Vectors and SIMD

Single Instruction Multiple Data (`SIMD`) is supported by the CPU

`@Vector`

Only support 
- bool
- float
- int
- pointer
[Code](https://codeberg.org/dude_the_builder/zig_in_depth/src/branch/main/36_vectors)

### Initialize
```
    const int_vec_a = @Vector(3, u8){ 1, 2, 3 };
    const int_vec_b = @Vector(3, u8){ 4, 5, 6 };
    const int_vec_c = int_vec_a + int_vec_b;
    std.debug.print("int_vec_c: {any}\n\n", .{int_vec_c});
```
### Scalar

Use `@splat` to initialize with scalars
```
	const twos: @Vector(3, u8) = @splat(2);
```

### To Scalar

Use `@reduce` to create a scalar

```
	// Use @reduce to obtain a scalar from a vector.
    // Supported ops: .Or, .And, .Xor, .Min, .Max, .Add, .Mul
    const all_true = @reduce(.And, bools_vec_a);
```

### Array index
```
    // You can also use array index syntax.
    std.debug.print("bools_vec_a[1]: {}\n\n", .{bools_vec_a[1]});
```
### @Shuffle

#### Reorder

Omit first vector, it will reorder the second

The mask contains the element to replace with
`2` => 'h'
`3` => 'e' 

```
    // Use @shuffle to change the order of vector elements.
    const a = @Vector(7, u8){ 'o', 'l', 'h', 'e', 'r', 'z', 'w' };
    const b = @Vector(4, u8){ 'w', 'd', '!', 'x' };

    // To shuffle within a single vector, pass undefined as the second argument.
    // Notice that we can re-order, duplicate, or omit elements of the input vector
    const mask1 = @Vector(5, i32){ 2, 3, 1, 1, 0 };
    const res1: @Vector(5, u8) = @shuffle(u8, a, undefined, mask1);
    std.debug.print("res1: {s}\n\n", .{&@as([5]u8, res1)})
```

### Select elements

```
    const a = @Vector(7, u8){ 'o', 'l', 'h', 'e', 'r', 'z', 'w' };
    const b = @Vector(4, u8){ 'w', 'd', '!', 'x' };

    // Combining two vectors
    const mask2 = @Vector(6, i32){ -1, 0, 4, 1, -2, -3 };
    const res2: @Vector(6, u8) = @shuffle(u8, a, b, mask2);
    std.debug.print("res2: {s}\n\n", .{&@as([6]u8, res2)});
```
Negatives select from the second vector (using index)
-1 selects the first element of the second vector
The output is `world`

## @select

Uses bools to determine which vector to select from
```
    const c = @Vector(4, u8){ 'x', 'i', 'j', 'd' };
    const d = @Vector(4, u8){ 's', 'b', 'm', 'z' };
    const mask3 = @Vector(4, bool){ false, true, false, true };
    const res3: @Vector(4, u8) = @select(u8, mask3, c, d);
    std.debug.print("res3: {s}\n\n", .{&@as([4]u8, res3)});
```

The output is `simd`

## Using namespace

Use struct for creating namespaces

```
const Numbers = struct{
	const pi: f64 = 3.14,
	var x: u8 = 42; 
}
```
Import acts like creating a struct

`usingnamespace` merges two namespaces (mixins)

```
const std = @import('std')
const MyStd = struct {
	usingnamespace std;
};

MyStd.debug.print("Gack", .{})
```
Can fine tune an api with `usingnamespace`
```
const libs = {
	usingnamespace @import("math.zig");
	usingnamespace @import("gui.zig");
	usingnamespace @import("http.zig");
};
```
Or can be
```
	pub usingnamespace @import("math.zig");
	pub usingnamespace @import("gui.zig");
	pub usingnamespace @import("http.zig");
```

## Customize namespace

The following only adds `getTimeout()` if the namespace has a positive timeout
```
pub fn Request(comptime timeout: usize) type {
    return struct {
        method: enum { get, post, put, delete },
        path: []const u8,
        body: ?[]const u8,
        timeout: usize = timeout,

        const Self = @This();

        pub usingnamespace if (timeout > 0)
            struct {
                pub fn getTimeout(self: Self) usize {
                    return self.timeout;
                }
            }
        else
            struct {};
    };
}
```

Can use with `@hasDecl` which tests if there is a parameter
## defer and errdefer

These help clean up spaghetti code

Example of creating a linked list that handles errors

`defer` is called in reverse order
`defer` will never be called if the thing to defer to is not called

### Errordefer with capture
```
errdefer |err| std.debug.print("Error: boom", .{})
```
## CodeGen

```
const run_gen_exe = b.addRunArtifact(gen_exe);
run_gen_exe.step.dependOn(&gen_exe.step);

// Add command line argument to our code generation program
// Also tells zig to use fib.zig as a build dependency
const output_file = run_gen_exe.addOutputFileArg("fib.zig);

// Pass fib-end build arg as a 
// command line arg for the code gen exec
run_gen_exe.addArg(fib_end);

// Write the zig generated code
const gen_write_file = b.addWriteFiles();

gen_writeFiles.addCopyFileToSource(output_file, "src/zib.fib");

const main_exe = b.addExecutable(.{...})

main_exe.step.dependOn(&gen_write_file.step)
```
### Arguements
```
var args = std.process.args()
// First is the name
_ = args.next();
const filename = args.next().?;

// 15:30
var file = try std::

```






`

