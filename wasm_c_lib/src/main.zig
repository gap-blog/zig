const std = @import("std");
const testing = std.testing;

export fn add(a: i32, b: i32) i32 {
    return a + b;
}

test "basic add functionality" {
    try testing.expect(add(3, 7) == 10);
}

const c = @cImport({
    @cInclude("bindings.h");
});

export fn round_number(number: f32) f32 {
    return c.doSomeCThing(number);
}

export fn hello() u8 {
    // var str = c.hello_from_c();

    // @ptrCast
    // var ret: *const u8 = @ptrCast(str);

    // const buffer: [*c]u8 = c.hello_from_c();
    // const str = std.mem.span(@as([*:0]const u8, @ptrCast(buffer)));

    // const as_ptr: [*:0]const u8 = c.hello_from_c();
    // const ret: [*:0]const u8 = c.hello_from_c();
    // const buffer: [*c]const u8 = c.hello_from_c();
    const buffer = c.hello_from_c();

    const zCopy: [:0]const u8 = std.mem.span(buffer);

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    const ret = allocator.dupeZ(u8, zCopy);

    return ret[0];
    // var i: u8 = buffer[0];
    // return i;

    // const ret = std.mem.span(buffer);
    // _ = ret;

    // return buffer[0];
}

export fn hello_from_zig() *const [14:0]u8 {
    return "Hello from zig";
}
