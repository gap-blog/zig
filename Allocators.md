
3 types of memory
- global - In code, read only
- stack
- heap

```zig
const say = std.fmt.allocPrint(allocator, "It's over {d}!!!", .{user.power});
defer allocator.free(say);
```
%%
Requires:
Test
Defer
Try
%%

%%
TODO: Handle memory error gracefully and continuing execution
Use catch and atomically back out and continue
%%

- [x] Pass to fuction
- [x] General Purpose Allocator
- [ ] ~~Double Free~~
- [ ] ~~Use After Free~~
- [x] Detect Leaks
- [x] Fixed Buffer Allocator
- [x] Area Allocator
- [ ] ~~C allocator~~
- [ ] ~~Wasm Allocator
- [x] Memory Pool
- [x] TestFailure
- [ ] Log Allocator
- [x] Page Allocator
- [x] checkAllAllocationFailures
- [ ] ~~FailingAllocator~~
- [x] Allocator with a struct
## Allocate Memory

https://trgwii.com/articles/zig-allocators/
https://igor84.github.io/blog/basics-of-allocating-and-using-memory/


When creating a function that allocates memory, use:
```
// Caller must free returned bytes with `allocator`.
public fn ...
```


```zig
// allocators/allocate.zig
const std = @import("std");
const expect = std.testing.expect;

test "allocation" {
    // Select an allocator to use
    // const allocator = std.heap.GeneralPurposeAllocator(.{}){};

    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = general_purpose_allocator.allocator();

    // Allocate 100 units of 8 bits (u8)
    const memory = try allocator.alloc(u8, 100);
    // Free the memory when done
    defer allocator.free(memory);

    // Verify there is 100 units
    try expect(memory.len == 100);
    // Verify the unit type is an array of 8 bits (u8)
    try expect(@TypeOf(memory) == []u8);
}
```

## General Purpose Allocator
`std.heap.GeneralPurposeAllocator` is a function, and since it's using PascalCase, we know that it returns a type.  Knowing that it returns a type, maybe this more explicit version will be easier to decipher:
```zig
const T = std.heap.GeneralPurposeAllocator(.{});
var gpa = T{};

// is the same as:

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
```
## Pass an Allocator to a function
```
// allocators/pass_allocator_to_function.zig
const std = @import("std");
const expect = std.testing.expect;

fn allocate_memory(allocator: std.mem.Allocator) ![]u8 {
    const memory = try allocator.alloc(u8, 100);

    return memory;
}

test "allocation" {
    // Select an allocator to use
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = general_purpose_allocator.allocator();

    // Allocate 100 units of 8 bits (u8)
    const memory = try allocate_memory(allocator);
    defer allocator.free(memory);

    // Verify there is 100 units
    try expect(memory.len == 100);
    // Verify the unit type is an array of 8 bits (u8)
    try expect(@TypeOf(memory) == []u8);
}
```
## ~~~Detect Double Free~~~

%% 
TODO: This crashes rather than errors 
Rewatch *Zig in depth - Allocators*
%%

```
// allocators/double_free.zig
const std = @import("std");
const expect = std.testing.expect;

fn allocate_memory(allocator: std.mem.Allocator) !void {
    // TODO: This crashes rather than errors
    const memory = try allocator.alloc(u8, 100);
    // Free the memory when done
    allocator.free(memory);
    allocator.free(memory);
}

test "allocation" {
    // Select an allocator to use
    // const allocator = std.heap.GeneralPurposeAllocator(.{}){};

    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = general_purpose_allocator.allocator();

    // Allocate 100 units of 8 bits (u8)
    try allocate_memory(allocator);
}
```

## ~~~Detect Use After Free ~~~
%% TODO: This passes, rather than erroring %%
```
// allocators/use_after_free.zig
const std = @import("std");
const expect = std.testing.expect;

fn allocate_memory(allocator: std.mem.Allocator) ![]u8 {
    const memory = try allocator.alloc(u8, 100);

    return memory;
}

test "allocation" {
    // Select an allocator to use
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = general_purpose_allocator.allocator();

    // Allocate 100 units of 8 bits (u8)
    const memory = try allocate_memory(allocator);
    allocator.free(memory);

    // TODO: This should error
    _ = memory.len;
}
```
## Detect Leaks
```
// allocators/detect_leak.zig
const std = @import("std");
const expect = std.testing.expect;

test "allocation" {
    var ok_result: bool = undefined;
    {
        var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
        const allocator = general_purpose_allocator.allocator();
        defer {
            const result = general_purpose_allocator.deinit();
            // std.debug.print("We can print the results\n{}\n", .{results});
            ok_result = result == .ok;
        }

        // Allocate 100 units of 8 bits (u8) and leak it
        _ = try allocator.alloc(u8, 100);
    }
    try expect(!ok_result);
}
```
## Allocate Memory on the Stack with Fixed Buffer Allocator
Allocates on stack
```
const std = @import("std");

test "fixed buffer allocator" {
    // If we know the required memory size in advance, we can create
    // a fixed-size array for it as a buffer.
    var buffer: [10]u8 = undefined;
    // And then use that buffer as the backing-store for a FixedBufferAllocator.
    var fix_buffer_allocator = std.heap.FixedBufferAllocator.init(&buffer);
    const allocator = fix_buffer_allocator.allocator();

    const memory = try allocator.alloc(u8, buffer.len);
    defer allocator.free(memory);

    try std.testing.expect(memory.len == 10);
}
```
## Free everything at once with Arena Allocator
https://www.huy.rocks/everyday/01-12-2022-zig-how-arenaallocator-works
```
const std = @import("std");

test "fixed buffer allocator" {

    // var arena_allocator = std.heap.ArenaAllocator;
    // const allocator = arena_allocator.allocator();

    var arena_allocator = std.heap.ArenaAllocator.init(std.heap.page_allocator); // Or c_allocator
    const allocator = arena_allocator.allocator();
    defer arena_allocator.deinit();

    const memory = try allocator.create([10]u8);

    try std.testing.expect(memory.len == 10);
}
```

## Avoid fragmentation with Memory Pool Allocator
```
// allocators/memory_pool.zig
const std = @import("std");
const expect = std.testing.expect;

// When allocated objects have the same type, use memory pool
// This is more efficient, since freed memory can be reused
test "memory pool: basic" {
    const MemoryPool = std.heap.MemoryPool;

    var pool = MemoryPool(u32).init(std.testing.allocator);
    defer pool.deinit();

    const memory_1 = try pool.create();
    const memory_2 = try pool.create();
    const memory_3 = try pool.create();

    // Assert uniqueness
    try std.testing.expect(memory_1 != memory_2);
    try std.testing.expect(memory_1 != memory_3);
    try std.testing.expect(memory_2 != memory_3);

    // Destroy memory_2 so it can be reused
    pool.destroy(memory_2);
    // Allocate the same size, this will reuse the memory_2
    // rather than requesting new memory
    const memory_4 = try pool.create();

    // Verify memory reuse
    try expect(memory_2 == memory_4);
}
```

## ~~Use Malloc with C Allocator~~
For high performance (but very few safety features!), [`std.heap.c_allocator`](https://ziglang.org/documentation/master/std/#std.heap.c_allocator) may be considered. This,however, has the disadvantage of requiring linking Libc, which can be done with `-lc`.

Useful to use external c tooling like valgrind
	See:  wqhttps://cryptocode.github.io/blog/docs/valgrind-zig/
## Build for Javascript with Wasm Allocator
Web Assembly (WASM) has it own unique memory requirements, which is what 

## Test out of Memory with Test Failure Allocator
```
// allocators/memory_failure.zig
const std = @import("std");
const expect = std.testing.expect;

fn allocate_memory(allocator: std.mem.Allocator) ![]u8 {
    const memory = try allocator.alloc(u8, 100);

    return memory;
}

test "allocation" {
    // Select an allocator to use.  This allocator will cause an error
    const allocator = std.testing.failing_allocator;

    // Allocate 100 units of 8 bits (u8)
    const memory = allocate_memory(allocator);
    try std.testing.expectError(error.OutOfMemory, memory);
}
```


## Memory Pool
```
// allocators/memory_pool.zig
const std = @import("std");
const expect = std.testing.expect;

// When allocated objects have the same type, use memory pool
// This is more efficient, since freed memory can be reused
test "memory pool: basic" {
    const MemoryPool = std.heap.MemoryPool;

    var pool = MemoryPool(u32).init(std.testing.allocator);
    defer pool.deinit();

    const memory_1 = try pool.create();
    const memory_2 = try pool.create();
    const memory_3 = try pool.create();

    // Assert uniqueness
    try std.testing.expect(memory_1 != memory_2);
    try std.testing.expect(memory_1 != memory_3);
    try std.testing.expect(memory_2 != memory_3);

    // Destroy memory_2
    pool.destroy(memory_2);
    // Allocate the same size, this will reuse the memory_2
    // rather than requesting new memory
    const memory_4 = try pool.create();

    // Verify memory reuse
    try expect(memory_2 == memory_4);
}

```
## Log Allocations with Logging Allocator

## Low Level Allocation with Page Allocator
Good for drivers, kernal work and custom allocators
```
const std = @import("std");

test "arena allocator" {
    var arena_allocator = std.heap.ArenaAllocator.init(std.heap.page_allocator); 
    const allocator = arena_allocator.allocator();
    defer arena_allocator.deinit();

    const memory = try allocator.create([10]u8);

    try std.testing.expect(memory.len == 10);
}
```
## Test for allocation failures with checkAllAllocationFailures

https://www.ryanliptak.com/blog/zig-intro-to-check-all-allocation-failures/

```

// check_all_allocation_failures.zig

const std = @import("std");

fn allocate_memory(allocator: std.mem.Allocator) ![]u8 {
    const memory = try allocator.alloc(u8, 100);

    errdefer allocator.free(memory);
    return memory;
}

fn memoryTest(allocator: std.mem.Allocator, result: usize) !void {
    // Allocate 100 units of 8 bits (u8)
    const memory = try allocate_memory(allocator);
    defer allocator.free(memory);
    // Comment in the following line to create a double free
    defer allocator.free(memory);

    try std.testing.expectEqual(@as(usize, result), memory.len);
}

test "allocation" {
    // Select an allocator to use
    const allocator = std.testing.allocator;

    try std.testing.checkAllAllocationFailures(
        allocator,
        memoryTest,
        .{ 100 },
    );
}

```
## Use an allocator with a structure

```
// allocators/struct_memory.zig
const std = @import("std");
const expect = std.testing.expect;
const print = std.debug.print;

const List = struct {
    const Node = struct {
        data: u8,
        next: ?*Node = null,
    };

    head: ?*Node = null,
    tail: ?*Node = null,
    allocator: std.mem.Allocator,

    fn init(allocator: std.mem.Allocator) List {
        return .{ .allocator = allocator };
    }

    fn push(self: *List, value: u8) !void {
        const node = try self.allocator.create(Node);
        node.* = .{ .data = value };

        if (self.tail) |*tail| {
            tail.*.next = node;
            tail.* = node;
        } else {
            self.head = node;
            self.tail = node;
        }
    }

    fn deinit(self: *List) void {
        var current = self.head;
        while (current) |n| {
            const next = n.next;
            self.allocator.destroy(n);
            current = next;
        }
        self.head = null;
        self.tail = null;
    }
};

fn allocate_memory(allocator: std.mem.Allocator) ![]List {
    const memory = try allocator.alloc(List, 100);

    errdefer allocator.free(memory);
    return memory;
}

test "allocation" {
    // Select an allocator to use
    const allocator = std.testing.allocator;

    var list = List.init(allocator);
    defer list.deinit();

    try list.push(12);
}

```


## Notes

## GeneralPurposeAllocator

The Zig standard library also has a general-purpose allocator. This is a safe allocator that can prevent double-free, use-after-free and can detect leaks. Safety checks and thread safety can be turned off via its configuration struct (left empty below). Zig's GPA is designed for safety over performance, but may still be many times faster than page_allocator.

Use for by default
When in `debug mode` or `Release Safe` it will detect memory leaks or double frees

```
	var gpa = std.heap.GeneralPurposeAllocator(.{}){};
	// Print out memory leaks...etc
	defer std.debug.print("GPA result: {}\n", .{gap.deinit()});
```

## Testing Allocator
Use for tests.  Will detect memory leaks and double frees
```
const testing = std.testing;
test "List: add" {
	var list = try IntList.init(testing.allocator);
	defer list.deinit();

	for (0..5) |i| {
		try list.add(@intCast(i + 10));
	}
	try testing.expectEqual(@as(usize, 5), list.pos);
	try testing.expectEqual(@as(i64, 10), list.items[0]);
}
```
## FixedBufferAllocator
FixedBufferAllocator uses the stack, which is faster than the heap

%% 
TODO: Does the FixedBufferAllocator free when exiting the function?
%%
```
fn catAlloc(
	allocator: std.mem.Allocator,
	a: []const u8,
	b: []const u8,
) ![]u8 {
	const bytes = try allocator.alloc(u8, a.len + b.len);
	std.mem.copy(u8, bytes, a);
	std.mem.copy(u8, bytes[a.len..], b);
	return bytes;
}

test "fba bytes" {
	const hello = "Hello ";
	const world = "world";

	var buf: [11]u8 = undefined;
	var fba = std.heap.fixedBufferAllocator.init(&buf);
	// does not need to use `defer fab.deinit` because it allocates on stack
	const allocator = fba.allocator();
	const result = try catAlloc(allocator, hello, world);
	// Technically, this is not needed since it is on the stack
	defer allocator.free(result);

	try std.testing.expectEqualString(hello ++ world, result);
}

```

``` 
// Caller must free returned slice with `allocator`
fs sliceOfAlloc(
	allocator: std.mem.Allocator,
	item: anytype,
	n:usize,
) ![]@TypeOf(item) {
	// n is onky known at runtime, unlike with an array
	const slice = try allocator.alloc(@TypeOf(item), n);
	// Loop through each element as a pointer
	for (slice) |*e| {
		// Dereference the pointer and copy
		e.* = item;
	}
	return slice;
}

test "fba structs" {
	const Foo = struct {
		a: u8 = 42,
		b: []const u8 = "Hello world",
	};
	const foo = Foo[];
	const n = 2;
	
	var buf: [n * @sizeOf(Foo)]u8 = undefined;
	var fba = std.heap.FixedBufferAllocator.init(&buf);
	const allocator = fba.allocator();
	
	const result = try sliceOfAlloc(allocator, foo, n);
	defer allocator.free(result);

	// `&[_]Foo{ foo, foo}`
	// `&[_]` Takes the address of the array, casting it to a slice
	// `Foo{ foo, foo}` creates an array with two elemements

	try std.testing.expectEqualSlices(Foo, &[_]Foo{ foo, foo}, result);
}
```

```
pub fn main() void {
	std.debug.print("Run zig build test --summary all\n", .{});
}
```

## Arena Allocator

Free everything all at once, normally at the end of the program.

Good for limited tasks like:
- web requests
- recursive calls

Profile a GeneralAllocator vs an ArenaAllocator
Code for the ArenaAllocator is at `AreaAllocator 14:15`
The arena is much faster

```
// A generic linked list 
pub fn List(comptime T: type) type {
	return struct {
		const Node = struct {
			allocator: std.mem.Allocator,
			dataL T,
			next: ?*Node,
			
			fn init(allocator: std.mem.Allocator, data: T) !*Node {
				const ptr = try allocator.create(Node);
				ptr.allocator = allocator;
				ptr.data = data;
				ptr.next = null;
				return ptr
			}
			fn deinit(self: *Node) void {
				if (self.next) |ptr| ptr.deinit();
				self.allocator.destory(self);
			}
		};

		const Self = @This();

		allocator: std.mem.Allocator,
		head: *Node,
		
		pub fn init(allocator: std.mem.Allocator, data: T) !Self {
			return .{
				.allocator = allocator,
				.head = try Node.init(allocator, data),
			};
		}
	
		pub fn deinit(self: *Self) void {
			self.head.deinit();			
		}

		pub fn append(self:: *Self, data: T) !void {
			var tail: *node = self.head;
			while (tail.next) |ptr|  tail = ptr;
			tail.next = try Node.init(self.allocator, data);
		}

		public fn lookup(self: Self, data: T) bool {
			var current: ?*Node = self.head;

			return while (current) |node_ptr| {
				if (node_ptr.data = data) break true;
				current = node_ptr.next;
			} else false;
		}
	}
}

const std = @import("std");
pub fn main() !void {
	// `GeneralPurposeAllocator(.{})` use defaults
	// `GeneralPurposeAllocator(.{}){}` create instance
	var gpa = std.heap.GeneralPurposeAllocator(.{}){};
	defer _ = gpa.deinit();
	const allocator = gpa.allocator();

	const iterations: usize = 100;
	const item_count: usize = 1_000;

	var time = try std.time.Timer.start();

	for (0..iterations) |_|  {
		// Create the list
		var list = try List(usize).init(allocator, 13);
		errdefer list.deinit();
		// Add items
		for (0..item_count) |i| try list.append(i); 
		list.deinit();
	}

	var took: f64 = @floatFromInt(timer.read());
	std.debug.print("Took: {d:.2} ms\n", .{took});
}

```
## C Allocator

std.heap.c_allocator

wrapper for malloc / free

## ~~Wasm Allocator~~

Describing use of  `std.heap.wasm_allocator` will be deferred to the WASM section

## Test Failure Allocator

```
test "Allocation failure" {
	const allocator = std.testing.failing_allocator;
	var list = List(u8).init(allocator, 42);
	try std.testing.expectError(error.OutOfMemory, list);
}
```

## Memory Pool Allocator

Use if allocating a lot of objects of the same size, since it can reuse memory.  Prevents memory fragmentation

```
test "memory pool: basic" {
	const MemoryPool = std.heap.MemoryPool;

	var pool = MemoryPool(u32).init(std.testing.allocator);
	defer pool.deinit();
	const p1 = try pool.create();
	const p2 = try pool.create();
	const p3 = try pool.create();

	try std.testing.expect(p1 != p2);
	try std.testing.expect(p1 != p3);
	try std.testing.expect(p2 != p3);

	pool.destroy(p2);
	const p4 = try pool.create();

	// Assert memory reuse
	try std.testing.expect(p2 == p4);
}
```
## Logging Allocator

```
	var gpa = std.heap.GeneralPurposeAllocator(.{}){};
	var logging_allocator = std.heap.loggingAllocator(gpa.allocator());
```

Prints out allocations to the console

## Page Allocator

```
std.heap.page_allocator
```

Mainly used for writing your own allocators.  Be careful on using since it always uses at least
one page of memory (the size is platform dependent)


