On windows I recommend using WSL so you have a 

The official website is [https://ziglang.org/](https://ziglang.org/)  In the menu bar there is link to learn (https://ziglang.org/learn/) which has the language reference (https://ziglang.org/documentation/master/) which contains the specifications for the language.  You can select which version of the language you are using.  

TODO: insert picture

It defaults to the master branch which is the latest and greatest.  It also has the reference for the standard libraries (https://ziglang.org/documentation/master/std/).
## Version Manager

The *Zig Version Manager* (https://www.zvm.app/) allows you to switch between versions of zig, which can be very helpful.  Zig is a language that is under rapid development and sometime introduces breaking changes.  This allows you to upgrade versions of zig and see if the latest version fixes or breaks anything you can use the version manager.  It is quick and simple to use.

It has the disadvantage of only supporting stable versions.  That means if you encounter a bug in the language and want to see if the developers and fixed it, you can not use it.  You must use the nightly build.

TODO: Create a walk through of using the version manager. 
## Pre built version

Nightly builds can be downloaded from https://ziglang.org/download/.  You must know the architecture of the machine you are using and then can select an archived file to download.  On windows or the macOS it will be a zip file.  On Linux it is an *xy* file.

Symbolic link:

```
cd /usr/local/bin
sudo ln -s ~/Downloads/zig-linux-x86_64-0.12.0/zig
```
or you can create an alias.

TODO: Example of an alias

TODO Walk through on Linux, Windows and Mac

```
zig version
```

## Forum

There is no official forum, but the one at https://ziggit.dev/ is popular and a good place to start and ask questions if you have any.  A list of more places to share information can be found at 
[Communities](https://github.com/ziglang/zig/wiki/Community)

## VS code
https://medium.com/@eddo2626/lets-learn-zig-2-setting-up-a-zig-development-environment-bc3fc3c30aca

## IO

[Using StdOut and StdIn](https://medium.com/@eddo2626/lets-learn-zig-1-using-stdout-and-stdin-842ee641cd)




