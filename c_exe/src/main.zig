const std = @import("std");
const c = @cImport({
    @cInclude("bindings.h");
});

pub fn main() !void {
    c.doSomeCThing();
}
