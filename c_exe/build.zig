const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const exe = b.addExecutable(.{
        .name = "hello-world",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    const cflags = [_][]const u8{""};

    exe.linkLibC();
    exe.addIncludePath(.{ .path = "src" });
    exe.addCSourceFile(.{ .file = std.Build.FileSource.relative("src/bindings.c"), .flags = &cflags });

    b.installArtifact(exe);
}
